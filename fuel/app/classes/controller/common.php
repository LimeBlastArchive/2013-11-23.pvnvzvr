<?php

/**
 * The Common Controller.
 *
 * Base controller from which the site is run
 *
 * @package  app
 * @extends  Controller_Template
 */
class Controller_Common extends Controller_Template
{

	/**
	 * The index action.
	 *
	 * Presents the user with the four different factions, and each of their vote buttons
	 *
	 * @access  public
	 * @return  void
	 */
	public function action_index()
	{
		$data = array();
		$this->template->title = 'Choose Your Faction';
		$this->template->content = View::factory('index', $data);
	}

	/**
	 * The vote action.
	 *
	 * Accepts the user's vote, then shows the results
	 *
	 * @access  public
	 * @return  void
	 */
	public function action_vote($faction)
	{
		$data = array();

		// Check if request was ajax, if so, change to ajax template
		if (Input::is_ajax())
		{
			$this->template->set_filename('template_ajax');
		}

		$this->template->title = 'Thank you for voting';
		$this->template->content = View::factory('vote', $data);
	}

	/**
	 * The 404 action for the application.
	 *
	 * @access  public
	 * @return  void
	 */
	public function action_404()
	{
		$this->response->status = 404;
		$this->template->title = 'Error 404: Page Not Found';
		$this->template->content = View::factory('404');
	}

	/**
	 * Designed to show the default skeleton view. Kept for reference only.
	 *
	 * @access  public
	 * @return  void
	 */
	public function action_reference()
	{
		$this->template->title = 'Reference';
		$this->template->content = View::factory('reference');
	}

}

/* End of file common.php */