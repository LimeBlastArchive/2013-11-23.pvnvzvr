<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>

	<!-- Basic Page Needs
	================================================== -->
	<meta charset="utf-8" />
	<title>
		<?php echo $title; ?>
		(Pirates Vs. Ninjas Vs. Zombies Vs. Robots!)
	</title>
	<meta name="description" content="">
	<meta name="author" content="Daniel Hollands - http://www.theworldofdan.co.uk/">
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Mobile Specific Metas
	================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- CSS
	================================================== -->
	<?php echo \Asset::css( array(
		'base.css',
		'skeleton.css',
		'layout.css',
	)); ?>
	<?php echo \Autoloader\Autoloader::get('css'); ?>

	<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="/assets/img/favicon.ico">
	<link rel="apple-touch-icon" href="/assets/img/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/assets/img/apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="/assets/img/apple-touch-icon-114x114.png" />

</head>
<body>

	<!-- Primary Page Layout
	================================================== -->
	<div class="container">
		<?php echo $content; ?>
	</div><!-- container -->

	<!-- JS
	================================================== -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
	<script src="http://cdn.jquerytools.org/1.2.5/jquery.tools.min.js"></script>
	<?php echo \Asset::js( array(
		'app.js'
	)); ?>
	<?php echo \Autoloader\Autoloader::get('js'); ?>

</body>
</html>